import React, { useState } from 'react';
import SelectionComponent from './SelectionComponent'
import './App.scss';
import ListComponent from './ListComponent';
import { getPrice } from './routes/apiController'
import { Button, Container, Alert, Form } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const App = () => {
  const [hasError, setError] = useState(false)
  const [state, setState] = useState({
    history: [
    ],
    timestamp: 0,
    value: undefined,
    fromCurrency: undefined,
    toCurrency: undefined
  })

  const updateFromToCurrency = (fromTo) => ({ value }) => {
    setState(state => ({ ...state, [fromTo]: value }))
  }

  const changeInputValue = (valueSelected) => {
    setState(state => ({ ...state, value: valueSelected }))
  }

  const performConvert = async () => {
    let resultApi = await getPrice(state.value, state.fromCurrency, state.toCurrency)
    if (resultApi.priceList.length === 0) {
      setError(true)
    }
    else {
      setError(false)
      let temp = resultApi.priceList.concat(state.history).slice(0, 10)
      setState(state => ({ ...state, history: temp, timestamp: resultApi.timestamp }))

    }
  }

  return (
    <Container fluid>
      <div className="app" >
        <div className="data-input">
          <Form.Control placeholder="Amount to Convert" type="number" id="value" min="0" onChange={(event) => changeInputValue(event.target.value)}></Form.Control>
          <SelectionComponent className='selector-component' updateFromCurrency={updateFromToCurrency("fromCurrency")} />
          <SelectionComponent className='selector-component' updateFromCurrency={updateFromToCurrency("toCurrency")} />
          <div className="error-alert">
            <Button disabled={!state.value || !state.fromCurrency || !state.toCurrency} onClick={() => { performConvert() }}>
              Convert
            </Button>
            {hasError && <Alert variant='danger'>Sorry, API is Offline</Alert>}
            {state.timestamp > 0 &&
              <Alert variant='info'>{"Last Check:" + new Date(state.timestamp)}</Alert>
            }
          </div>
        </div>
        <ListComponent history={state.history} timestamp={state.timestamp}></ListComponent>
      </div>
    </Container>
  );
}

export default App;
