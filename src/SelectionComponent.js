import React from 'react';
import Select from 'react-select';
import { func } from 'prop-types';
import options from './resources/options.json'

const SelectionComponent = (props) => (
    <Select
        placeholder="Currency to Convert"
        className="basic-single"
        classNamePrefix="select"
        isDisabled={false}
        isLoading={false}
        isClearable={false}
        isRtl={false}
        isSearchable={true}
        options={options}
        onChange={props.updateFromCurrency}
    />
)

SelectionComponent.propTypes = {
    updateFromCurrency: func.isRequired,
}
export default SelectionComponent