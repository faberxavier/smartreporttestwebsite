
export const getPrice = async (value, fromCurrency, toCurrency) =>
    await fetch(`http://localhost:3100/conversion?value=${value}&fromCurrency=${fromCurrency}&toCurrency=${toCurrency}`)
        .then(res => res.json())
        .then((data) => ({
            priceList: data.rates,
            timestamp: data.timestamp
        }))
        .catch(() => ({ priceList: [], timestamp: 0 }))

