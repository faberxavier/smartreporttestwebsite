import React from 'react';
import toJson from 'enzyme-to-json';
import { mount, shallow } from 'enzyme';
import { render } from '@testing-library/react';
import App from '../App';

describe('App', () => {
  it('should render correctly', () => {
    const wrapper = mount(<App />);
    const tree = toJson(wrapper);
    expect(tree).toMatchSnapshot();
  });
});