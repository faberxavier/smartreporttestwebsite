import React from 'react';
import toJson from 'enzyme-to-json';
import { mount } from 'enzyme';
import ListComponent from '../ListComponent';

describe('ListComponent', () => {
    it('should render correctly when no history is passed', () => {
        const wrapper = mount(<ListComponent history={[]} />);
        const tree = toJson(wrapper);
        expect(tree).toMatchSnapshot();
    })

    it('should render component when history is passed', () => {
        const props = [{
            toCurrency: 'ALF',
            fromCurrency: 'BRL',
            price: 152
        },
        {
            toCurrency: 'BRL',
            fromCurrency: 'USD',
            price: 52
        }]
        const wrapper = mount(<ListComponent history={props} />);
        const tree = toJson(wrapper);
        expect(tree).toMatchSnapshot();
    })
});