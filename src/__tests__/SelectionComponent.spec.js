import React from 'react';
import toJson from 'enzyme-to-json';
import { mount } from 'enzyme';
import SelectionComponent from '../SelectionComponent';

describe('SelectionComponent', () => {
    it('should render correctly', () => {
        const wrapper = mount(<SelectionComponent updateFromCurrency={jest.fn()} />);
        const tree = toJson(wrapper);
        expect(tree).toMatchSnapshot();
    })
});