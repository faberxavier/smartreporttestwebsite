import CurrencyFlag from 'react-currency-flags';

import React from 'react'
import names from './resources/names.json'
import './ListComponent.scss'
import { arrayOf, string, number, shape } from 'prop-types';

const ListComponent = ({ history }) => (

    <table className="responstable">
        <thead>
            <tr>
                <th>Flag</th>
                <th>From</th>
                <th>Flag</th>
                <th>To</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            {
                (history && history.length > 0) &&
                history.map((listItem, index) =>
                    names[listItem.toCurrency] &&
                    <tr key={index}>
                        <td><CurrencyFlag currency={listItem.fromCurrency} size="md" className="itemList" /></td>
                        <td><div className="itemList">{names[listItem.fromCurrency]}</div></td>
                        <td><CurrencyFlag currency={listItem.toCurrency} size="md" className="itemList" /></td>
                        <td><div className="itemList">{names[listItem.toCurrency]}</div></td>
                        <td> <div className="itemList">{listItem.price + " " + listItem.toCurrency}</div></td>
                    </tr>
                )
            }
        </tbody>
    </table>
)

ListComponent.propTypes = {
    history: arrayOf(
        shape({
            toCurrency: string.isRequired,
            fromCurrency: string.isRequired,
            price: number.isRequired
        })
    ).isRequired
}
export default ListComponent
